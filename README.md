### Tired of newbies joining your deathwish or overkill lobby, feeding on your ammo/meds getting downed every 5 seconds while carrying single ecm? ######
### Why not auto-manage your lobby? ######
To be honest since Overkill cannot make an infamy filter or something else than level filter. I came up with an idea of creating total hours played filter.

---------------------------------------

Features:

* Hours playtime
* Perk deck
* Perk deck completion
* Armor
* Deployables
* Heists completed
* Heists completed on deathwish
* Pd2stats anti-cheat server
* Skill cheats (has too many skill points)
* Blacklist (banned people list)
* Infamy (if the person has private profile)

Additional notes:

* Only active on deathwish and overkill lobbies.
* When the game starts, game will output everyones skills to you.
* Outputs number of hours person played to the game chat.
* Generates whitelist straight from your steam friend list.
* Cheaters that join your lobby will be kicked automatically and added into blacklist for future heists and their ban reason will be outputed to chat/blacklist file.

![Example of player filtering](http://lastbullet.net/mydownloads/previews/preview_13030_1448976452_a254db939bfda6cf69f01c76670a5789.jpg)
![Ingame skill printing](http://lastbullet.net/mydownloads/previews/preview_13030_1451552753_167c0365410bde25fd03d9c3ccb812f6.jpg)
![Inspection menu](http://lastbullet.net/mydownloads/previews/preview_13030_1463426646_5e415f24686b0d24f5db9c814c0495a0.jpg)
![Example of blacklist](http://lastbullet.net/mydownloads/previews/preview_13030_1451581122_0a9a4a2052eadc4875e1597cc371ab2a.jpg)
![Private profile handling](http://lastbullet.net/mydownloads/previews/preview_13030_1451737742_1542fa597bb8dd26f6bd360c72c91f21.jpg)